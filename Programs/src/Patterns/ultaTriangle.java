package Patterns;

public class ultaTriangle {
	public static void main(String[] args) {
		int n= 4;		
		//Simple
		
		
		for ( int i=0 ;i<=n;i++){
			for ( int j=0;j<=i;j++){
				System.out.print("*"); 
				}
			System.out.println("");
		}
		/*
        *
		**
		***
		****
		******/
		System.out.println("------------------------");
		/*Ulta*/
		for(int i=n;i>=1;i--){
			for(int j=1;j<=i;j++){
				System.out.print("*");
			}
			System.out.println("");
		}
	/*	****
		***
		**
		*
		**/
		
		System.out.println("------------------------");
		/*
		 * 1
           12
           123
           1234
		 */
		for ( int i=1 ;i<=n;i++){
			for ( int j=1;j<=i;j++){
				System.out.print(j+""); 
				}
			System.out.println("");
		}
		//22
		//333
		System.out.println("------------------------");
		for ( int i=1 ;i<=n;i++){
			for ( int j=1;j<=i;j++){
				System.out.print(i+""); 
				}
			System.out.println("");
		}
		System.out.println("------------------------");
		/*Upar Niche
		1
		12
		123
		1234
		123
		12
		1*/
		for ( int i=1 ;i<=n;i++){
			for ( int j=1;j<=i;j++){
				System.out.print(j+""); 
				}
			System.out.println("");
		}
		for (int i =n-1;i>=1;i--){
			for(int j=1;j<=i;j++){
				System.out.print(j+"");
			}
			System.out.println("");
		}

	}

}
