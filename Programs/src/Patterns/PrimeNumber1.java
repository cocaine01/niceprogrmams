package Patterns;

public class PrimeNumber1 {

	public static void main(String[] args) {
		
		int n =25;
		
		int cont =0;
		
		for(int i=2;i<=n;i++) {
			if(n%i==0) {
				cont++;
				break;
			}
		}
		if(cont==0) {
			System.out.println("Prime");
			
		}
		else {
			System.out.println("Not Prime");
		}
		System.out.println(cont);
	}

}
