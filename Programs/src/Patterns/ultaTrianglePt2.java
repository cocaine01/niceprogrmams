package Patterns;

public class ultaTrianglePt2 {

	public static void main(String[] args) {
		int n=6;
		int i,j;
		int k = (2*n)-2;
		///System.out.println(k);
		
		for ( i=0; i<n; i++)
		{

			// inner loop to handle number spaces
			// values changing acc. to requirement
			for ( j=0; j<k; j++)
			{
				// printing spaces
				System.out.print("/");
			}
			

			// decrementing k after each loop
			k = k - 2;

			// inner loop to handle number of columns
			// values changing acc. to outer loop
			for ( j=0; j<=i; j++ )
			{
				// printing stars
				System.out.print("* ");
			}

			// ending line after each row
			System.out.println();
		}
	
		
		
		
			
		
	/*	xxxx* 
		xx* * 
		* * * */
		
		//k=4
/*		for(i=0; i<n; i++)
		{

			// inner loop to handle number spaces
			// values changing acc. to requirement
			for(j=0; j<k; j++)
			{
				// printing spaces
				System.out.print("/");
			}

			// decrementing k after each loop
			k = k - 2;

			//  inner loop to handle number of columns
			//  values changing acc. to outer loop
			for(j=0; j<=i; j++)
			{
				// printing stars
				System.out.print("* ");
			}

			// ending line after each row
			System.out.println();
		}*/
		}
}
		
		
		