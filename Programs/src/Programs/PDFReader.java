package Programs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.cert.CertPathValidatorException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class PDFReader {
	
	

	public static void main(String[] args) throws IOException, CertPathValidatorException {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\main_pdf.pdf");
		File output = new File(System.getProperty("user.dir")+"\\Data\\trial.txt");
		PDDocument pd = PDDocument.load(fis);
		
		System.out.println(pd.getNumberOfPages());
        System.out.println(pd.isEncrypted());
        pd.save("CopyOfInvoice.pdf"); // Creates a copy called "CopyOfInvoice.pdf"
        PDFTextStripper stripper = new PDFTextStripper();
        BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output)));
        stripper.writeText(pd, wr);
        if (pd != null) {
            pd.close();
        }
       // I use close() to flush the stream.
       wr.close();
       
       
       
       
       
       
} 
    
}


