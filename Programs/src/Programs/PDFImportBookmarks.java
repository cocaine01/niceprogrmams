package Programs;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.security.cert.CertPathValidatorException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineNode;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class PDFImportBookmarks {

	public static void printBookmark(PDOutlineNode bookmark, String indentation) throws IOException
	{
		PDOutlineItem current = bookmark.getFirstChild();
		while (current != null)
		{
			System.out.println(indentation + current.getTitle());
			printBookmark(current, indentation + "    ");
			current = current.getNextSibling();
		}
	}
	
	public static void csvToXLSX(String csvFileAddress  ,String xlsxFileAddress) {
	    try {
//	        = "test.csv"; //csv file address
//	         = "test.xlsx"; //xlsx file address
	        XSSFWorkbook workBook = new XSSFWorkbook();
	        XSSFSheet sheet = workBook.createSheet("sheet1");
	        String currentLine=null;
	        int RowNum=0;
	        BufferedReader br = new BufferedReader(new FileReader(csvFileAddress));
	        while ((currentLine = br.readLine()) != null) {
	            String str[] = currentLine.split(",");
	            RowNum++;
	            XSSFRow currentRow=sheet.createRow(RowNum);
	            for(int i=0;i<str.length;i++){
	                currentRow.createCell(i).setCellValue(str[i]);
	            }
	        }

	        FileOutputStream fileOutputStream =  new FileOutputStream(xlsxFileAddress);
	        workBook.write(fileOutputStream);
	        fileOutputStream.close();
	        System.out.println("Done");
	    } catch (Exception ex) {
	        System.out.println(ex.getMessage()+"Exception in try");
	    }
	}

	public static void main(String[] args) throws CertPathValidatorException, IOException {
		
		
		
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\main_pdf.pdf");
		//FileWriter fos = new FileWriter(new File(System.getProperty("user.dir")+"\\Data\\trial.txt"));		
		PDDocument pdf = PDDocument.load(fis);
		PDDocumentOutline outline =  pdf.getDocumentCatalog().getDocumentOutline();
		printBookmark(outline, "");
		pdf.close();
		
		
		String csvFileAddress = System.getProperty("user.dir")+"\\Data\\output.csv";
		System.setOut(new PrintStream(new FileOutputStream(csvFileAddress)));
		FileInputStream fis1 = new FileInputStream(System.getProperty("user.dir")+"\\Data\\main_pdf.pdf");
		//FileWriter fos = new FileWriter(new File(System.getProperty("user.dir")+"\\Data\\trial.txt"));		
		PDDocument pdf1 = PDDocument.load(fis1);
		PDDocumentOutline outline1 =  pdf1.getDocumentCatalog().getDocumentOutline();
		printBookmark(outline1, "");
		pdf1.close();
		
		String xlsxFileAddress= System.getProperty("user.dir")+"\\Data\\outputExcel.xlsx";
		
		//PDFImportBookmarks.csvToXLSX(csvFileAddress, xlsxFileAddress);
		
		
		
		//System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream("C:/Users/sumeetk/Desktop/Selenium/GIT Local Repository/Programs/console.out")), true));
        
		

//		File output = new File(System.getProperty("user.dir")+"\\Data\\trial.txt");
//		FileOutputStream fos = new FileOutputStream(output);
//		PrintStream ps = new PrintStream(fos);
	}
}
       
		
		
//		FileOutputStream foss = new FileOutputStream(System.getProperty("user.dir")+"\\Data\\trial.txt"));
//	    //we will want to print in standard "System.out" and in "file"
//	    TeeOutputStream myOut=new TeeOutputStream(System.out, foss);
//	    PrintStream ps = new PrintStream(myOut, true); //true - auto-flush after println
//	    System.setOut(ps);


		


		//      *************TO GET THE INDEXES******** ///		
		//		PDDocumentOutline outline =  pdf.getDocumentCatalog().getDocumentOutline();
		//		PDOutlineItem item = outline.getFirstChild();







		
		

	



