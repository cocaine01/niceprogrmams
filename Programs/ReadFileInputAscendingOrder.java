package Programs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class ReadFileInputAscendingOrder {

	public static void main(String[] args) throws IOException {
		
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\Programs.txt");
		
		int data =fis.read(); ///byte stream coming into data
		//System.out.println((char)data);
		
		StringBuffer sb = new StringBuffer();		
		
		while (data!= -1){
			char c= ((char)data);
			//System.out.println(c);
			data =fis.read();
			
			sb.append(c);
			
			}
        System.out.println(sb);
        
        //Ascending :- 1st method 
        
               String [] arr =  sb.toString().split(" ");
               
               for(String str:  arr){
            	   System.out.println(str);
               }
               
//               Arrays.sort(arr);
//               
//               System.out.println("\\n");
//               
//               for(String str:  arr){
//            	   System.out.println(str);
//               }
        //2nd Method
               
               Set<String> ts = new TreeSet<>();
               
               ts.add(str);
               
               
//               for (String str : arr){
//            	   ts.add(str);        //internally uses CompareTo method of Comparable Inrterface    	   
//               }
               System.out.println("ts ki value"+ts);
               
               Set<Integer> ts_desc = new TreeSet<>(new MyComparator());
               
               for (int i = 0 ; int <=15; i++ ){
            	   
            	   ts_desc.add(i);
            	   
               }
               
               System.out.println(ts_desc);
	}
	//decending
	
	

}
