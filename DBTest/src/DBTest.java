import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import  java.sql.ResultSet;


public class DBTest {
	
	public static Connection CONN = null;
	public static Statement stmt;
	public static ResultSet rs;
	
	

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		
		String dbUrl = "jdbc:mysql://localhost:3306/world";		
	
		String username = "root";	
		String password = "password";				
	
		String query = "select * from city where district='Kabol';";
				
				 	
	       	    Class.forName("com.mysql.jdbc.Driver");			
	       
	       		//Create Connection to DB		
	        	try{
	        		CONN = DriverManager.getConnection(dbUrl,username,password);
	      
	      		//Create Statement Object		
	    	   stmt= CONN.createStatement();					
	   
	   			// Execute the SQL Query. Store results in ResultSet		
	     		rs= stmt.executeQuery(query);
	     		
	     		System.out.println(rs);
	     		
	     		while(rs.next()){
	     			
	     			System.out.println(rs.getString("Name") + "\t" + rs.getString("District"));
	     		}
	     		
	        	}
	        	finally{
	        		if(CONN != null){
	        			CONN.close();
	        		}
	        		
	        		if(rs != null){
	        			rs.close();
	        		}
	        		
	        		if(stmt != null){
	        			stmt.close();
	        		}
	        	}
	     		
	     		
	     		
	     		

	}

}
