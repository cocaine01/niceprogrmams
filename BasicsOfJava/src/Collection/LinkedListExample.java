package Collection;

import java.util.LinkedList;
import java.util.List;

public class LinkedListExample {
	
	/*
	 * 1) As explained above the insert and remove operations give good performance (O(1)) 
	      in LinkedList compared to ArrayList(O(n)). Hence if there is a requirement of frequent addition
	      and deletion in application then LinkedList is a best choice.

       2) Search (get method) operations are fast in Arraylist (O(1)) but not in LinkedList (O(n)) 
       so If there are less add and remove operations and more search operations requirement, ArrayList would be your best bet.
	 */

	public static void main(String[] args) {
		
        List<String> ll= new LinkedList<>();        
        ll.add("abc");
        ll.add("xyz");        
        System.out.println(ll);
        
        List<String> ll2= new LinkedList<>();
        
        ll2.add("abc1");
        ll2.add("xyz1");
        ll2.add("xyz");
        ll2.addAll(ll);
        System.out.println(ll2);
        System.out.println("--------------------");
        
        ll2.add(2, "arg");
        System.out.println(ll2);
        System.out.println("--------------------");
        
        ll2.removeAll(ll);
        System.out.println(ll2);
        System.out.println("--------------------");
        System.out.println("Is the List empty:-" +ll2.isEmpty());
        
	}

}
