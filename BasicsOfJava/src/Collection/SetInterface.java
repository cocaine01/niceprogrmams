package Collection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetInterface {
	
	
	/*
	 * Although if you add an element two times , it will still show you the element once
	 * No maintenance of Insertion order
	 * Difference between List and Set

       No Duplicate in Set and Insertion Order not maintained in HashSet
       Although if you add an element two times , it will still show you the element once

       Since Insertion Order is not maintained in HashSet, so came in LinkedHashSet 
       and in Tree Set it is maintained in Ascending Order
	 */

	public static void main(String[] args) {
		
		Set<Integer> set=new HashSet<Integer>();//--------------HashSet
		
		System.out.println(set.add(10));
		System.out.println(set.add(10));//--returns false
		set.add(12);
		set.add(10);
		set.add(11);		
		System.out.println("Set 1 is :-"+set);    
		
		Set<Integer> set2=new HashSet<Integer>();
		
		set2.add(13);
		set2.add(16);
		set2.add(119);
		
		System.out.println("Set 2 is :-"+set2);
		
		System.out.println("----------------------------");
		
		set2.addAll(set);
		System.out.println("Set 2 is :-"+set2);
		System.out.println("----------------------------");
		System.out.println(set2.size());
		
		System.out.println("------------LINKED HASHSET----------------");
		Set<Integer> LinkedHashset = new LinkedHashSet<Integer>();///LinkedHashSet
		
		LinkedHashset.add(10);
		LinkedHashset.add(20);
		LinkedHashset.add(50);
		LinkedHashset.add(50);
		LinkedHashset.add(40);
		
		System.out.println(LinkedHashset);
		
		System.out.println("------------ TreeSET- Data In ascending Order---------------");
		Set<Integer> TreeSet = new TreeSet<Integer>();///TreeSet
		
		TreeSet.add(10);
		TreeSet.add(20);
		TreeSet.add(50);
		TreeSet.add(50);
		TreeSet.add(40);
		
		System.out.println(TreeSet);
		
		
		
		
	}

}
