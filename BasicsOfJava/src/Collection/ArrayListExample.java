package Collection;

import java.util.ArrayList;
import java.util.List;



public class ArrayListExample {
	
	
	/*
	 * Can insert duplicate elements
	 * InsertionOrder is preserved
	 * 
	 */

	public static void main(String[] args) {
		
		//list me not specifying the type of data that can be inserted
		List<Object> al = new ArrayList<>();
		
		//to remove the warning use Object as input , may need type casting
		//List<Object> al = new ArrayList<>();
		
		al.add(2);
		al.add("sumeet");
		al.add(9.8);
		al.add(true);
		al.add(2);
		
		System.out.println(al);
		System.out.println("----------------------------------");	
		System.out.println("Element at index 2 is " + al.get(2));
		System.out.println("----------------------------------");	
		System.out.println("Does arraList contain sumeet :-" +al.contains("sumeet"));
		System.out.println("***************");
		System.out.println("Size of the arrayList is :- " +al.size());		
		System.out.println("----------------------------------");	
		
		
		 List<Integer> al2= new ArrayList<Integer>() ;		 
			al2.add(2);
			al2.add((int) 2.2);			
			System.out.println(al2);
		
			
		System.out.println("----------------------------------");	
        List <Integer>  al3= new ArrayList<Integer>();
        
        al3.add(0);
       // al3.addAll(al);
        al3.add(3,7); ///adding at Index location
        
        System.out.println(al3);
        
        System.out.println("----------------------------------");	
        
        //removeAll
         al2.removeAll(al);
         System.out.println(al2);
         
         System.out.println("-----------------");
         al.removeAll(al2);
         System.out.println(al);
        		
		
	}

}
