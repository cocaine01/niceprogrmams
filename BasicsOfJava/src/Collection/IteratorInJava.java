package Collection;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

public class IteratorInJava {

	
	/*
	 * Iteratate the data
	 * Iterator,ListEnumerator Enumerator,
	 * 3 methods :- hasNext();
	                next(); 
	                remove();
	 * Print all the data of collection
	 * Iterator:- hasNext(), next() and remove()
	   Enumerations:-  hasMoreElements() and nextElement()
	 * Enumerator works on Vector only
	 * 
	 * Using Iterator, you can remove an element of the collection while traversing it.But not with enumerations.
	 * 
	 */
	
	public static void main(String[] args) {
		
		List<Integer> ai = new LinkedList<>();
		
		ai.add(2);
		ai.add(3);
		ai.add(4);
		ai.add(5);
		
		System.out.println(ai);
		
		ListIterator<Integer> itr2 = ai.listIterator(ai.size());
		while(itr2.hasPrevious()){
			System.out.println(itr2.previous());
		}
		
		
		
		
		
		
		
		
        List<Integer> vector=new Vector<>();
		
		vector.add(10);	
		vector.add(10);	
		vector.add(20);	
		vector.add(30);	
		
		System.out.println(vector);
		
		Iterator<Integer> itr = vector.iterator();
		while (itr.hasNext()){
			System.out.println(itr.next());
		}
		
		
		System.out.println("------------------------");
		
		//List Iterator()
		List<String> ll= new LinkedList<>();        
        ll.add("abc");
        ll.add("xyz");        
        System.out.println(ll);
      
        //ListIterator<String> li= ll.listIterator();
        
//        System.out.println("------------Forward------------");
//        while (li.hasNext()){
//        	System.out.println(li.next());
//        }
        ListIterator<String> li= ll.listIterator(ll.size());//way to handle previous
		System.out.println("----------Reverse Direction:- List Iterator has One more method--previous...must have pointer to last then only it will work, so use hasNext then only previous--------------");
        while (li.hasPrevious()){
        	
        	System.out.println(li.previous());        	
        }
        
        System.out.println("-------------------------------");
        //ENUMERATOR
        /*
         * 
         */
        
        List<Integer> vectorE=new Vector<>();
		
		vectorE.add(10);	
		vectorE.add(10);	
		vectorE.add(20);	
		vectorE.add(30);	
        
       Enumeration<Integer> enu= ((Vector<Integer>) vectorE).elements();
       
        while (enu.hasMoreElements()){
        	System.out.println(enu.nextElement());
        }

	}

}
