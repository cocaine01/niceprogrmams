package Collection;

import java.util.List;
import java.util.Vector;

public class VectorInJava {
	
	/*
	 * Vector is legacy class
	 * Vector is THREAD SAFE , so no DATA CORRUPTIONS
	 * Allowas duplicate Data, maintains Insertion Order
	 * 
	 */

	public static void main(String[] args) {
		
		List<Object> vector=new Vector<>();
		
		vector.add(10);	
		vector.add(10);	
		vector.add(20);	
		vector.add(30);	
		System.out.println(vector);
		
		List<Double> vector2=new Vector<>();
		vector2.add((double) 10);	
		vector2.add(Double.valueOf(20)); ///type casting
		System.out.println(vector2);
		
		System.out.println("-----------------");
		System.out.println("Is vector Empty ??? :- "+vector.isEmpty());
		System.out.println("-----------------");
		System.out.println("Size of the vector:-"+vector2.size());
		System.out.println("-----------------");
	    vector.addAll(vector2);
	    System.out.println("After addition"+vector);
		System.out.println("-----------------");
		
		
		
	}

}
