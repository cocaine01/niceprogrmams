package Exceptions;

public class TryCatch {
	
	
	static int b;
	
	public static void syntax(){
		
		try{
			 b=90/0;
		}
		catch (Exception e){
			System.out.println("Catch block to handle the exception");
		}
	}
	
	public static void multipleCatch(){
		try {
			int a[]=new int[5];
			
			a[5]= 40/2;
			
		}
		catch (ArithmeticException e){
			System.out.println("Arithmetic Exception aaya");
		}
		catch (ArrayIndexOutOfBoundsException e){
			System.out.println("ArrayIndex OutOfBounds Exception aaya");
			e.printStackTrace();
		}
		catch(Exception e){
			System.out.println("Ye hamesha last me likhna chahiye as it is superclass of all");
		}
		//
		finally{
			System.out.println("------------------------------");
			System.out.println("ye FINALLY wala execute hoga hi hoga and try without catch can be written with finally");
		}
		
	}
	
	/*
	 * Throw is used to explicitly throw an exception
	 * we can throw either checked or unchecked exception in Java
	 * Used to throw custom exception
	 */
	
	
	public static void throwTest(int age){
		if(age < 40) 
			throw new ArithmeticException("not valid");
		else 
			System.out.println("its valid");
		
	}
		
	
	

	public static void main(String[] args) {
		
         syntax();
         
         System.out.println("---------------------------------------------");
         multipleCatch();
         
         System.out.println("---------------THROW------------------------------");
         
         throwTest(20);
	}

}
