package Exceptions;

import java.io.IOException;

public class Throws {
	
	
	/*
	 * Throws to declare the exception
	 */
	
	public static void test1(int age){
		//for Unchecked notice kiya maine ki throw chalta hai
		if(age <70)
		throw new ArithmeticException("IO error");
		
	}
	
	
	public static void test2Throws () throws IOException{
		
		throw new IOException("IO Exception");
	}
	
//	public static void test3() throws IOException{
//		test2Throws();
//	}
	
	public static void test3() throws IOException {
		test2Throws();
	}
	
	public static void test4(){
		
		try{
			test3();
		}
		catch (Exception e){
			System.out.println("Exception is handled by master class");
		}
	}
	
	

	public static void main(String[] args) {
		test4();

	}

}
