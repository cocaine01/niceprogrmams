package Exceptions;

public class One {
	
	/*
	 * 2 types --Checked(can be handled at the compile time--IO and SQL) and 
	   Unchecked exception( Run Time exception-arithmetic
	 * NullPointer,ArrayIndexOutofBounds). ERROR is Different thing but it also extends Throwable.
	 * All are classes.
	 * All checked and Unchecked are subset of superClass EXCEPTION and Exception extend throwable and Throwable extend to OBJECT.
	 * execution will not get halted .
	 * 
	 * 
	 * Types of exception:-
	 * Arithmetic- divide by 0
	 * Null Pointer - not initialized and then directly working on the same
	 * Stack Overflow- memory issue, infinite loop addition in an array
	 * Number format exception- parsing a String to Integer
	 * FileNotFound-- Excel sheet not found
	 * ClassNotFound:- in SQL connection forName
	 * OutofMemory----
	 * ClassCASTEXCEPTION-----casting of Parent class object with Child Class
	 * illegalStateException:- Scanner closed and try to get next line
	 * 
	 * 
	 * Tirath:- all exceptions can be handled by these 5:- Try, catch, finally, throw and throws.
	 */
	
	
	

	public static void main(String[] args) {
	

	}

}
