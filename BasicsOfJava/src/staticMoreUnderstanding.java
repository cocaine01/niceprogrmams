
public  class staticMoreUnderstanding {
	
	int i;
    static int y;
    
    
    public void testNonStaticMethod(){
    	System.out.println("Test Non Static");    	
    	//Non Static Method can access both static and not static variables 
    	System.out.println(i);
    	System.out.println(y);
    }

    public static void testStaticMethod(){
     System.out.println("Static Method");     
     //STatic method can access only the  static variables  else compile time error
     //System.out.println(i);--compile time error
     System.out.println(y);
    }
    
    
	public static void main(String[] args) {
		 //class accessing the static variables and static Methods directly without creating the object reference
		System.out.println(staticMoreUnderstanding.y);
		staticMoreUnderstanding.testStaticMethod();
		
		//object creation of the class and then accessing both Static and Non-static methods
		
		staticMoreUnderstanding obj=new staticMoreUnderstanding();
		obj.testNonStaticMethod();
		obj.testStaticMethod();
		/* Not a good Practice to access the static variables or methods by object as the static 
		members are executed only once and if done it will occupy the heap memory which is unneccessary */
		
		System.out.println(obj.i);
		System.out.println(obj.y);
		    

	}
}
/*IMPORTANT NoTES*
//Non Static Method can access both static and not static variables 
//STatic method can access only the  static variables  else compile time error
//class accessing the static variables and static Methods directly without creating the object reference
//object creation of the class and then accessing both Static(Not a good Practice) and Non-static methods
/* Not a good Practice to access the static variables or methods by object as the static 
members are executed only once and if done it will occupy the heap memory which is unneccessary */
