package This;

public class This {

	///this refer to current class instance variables
	// this is used to invoke current class constructor
	// this is not available for static variable/members
	// you cannot call a constructor with an object. obj2.This not possibble

	///global or instance variable
	int a;
	int b;
	int c;
	static int d;

	public This(){
		System.out.println("Default");
	}

	public This(int a, int b){
		this.a=a; //Mapping between local and instance variable and initializing the variable
		this.b=b;
		System.out.println("RK");

	}

	public This(int a, int b,int c){
		//this(); error as already a parameterized has been initialized
		this(4,5);///its taking the first parameterized constructor
		//this(a,b);
		this.c=c;
		System.out.println("trial");
	}

	void display(){
		System.out.println("Value of a,b,c is   :" +a+","+b+","+c);
	}
	public static void main(String[] args) {

		//This obj= new This();
		This obj2= new This(9,10,15);
		obj2.display();	
	
	}

}
