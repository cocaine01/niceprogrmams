package This;

public class This1 {
     
	//this is used to call the non static methods
	//this can be used as a method argument
	
	int a;
	int b;
	int c;
	static int d;
	
	This1(){
		System.out.println("Default constructor");
	}
	
	void test1(){
		this.test2(); //this is used to call the non static methods
		System.out.println("Test 1");
	}
	
	void test2(){
		System.out.println("Test 2");
	}
	
	static void test3(int a){
		System.out.println("Test 3 Static");
	}
	
	//Variable obj which has type class
	void test4(This1 obj){
		System.out.println("Method is invoked");
		System.out.println(obj.getClass().getName());///package and class name
	}
	
	void test5(){
		this.test4(this);// invoking other method
		test3(10);
	}
	public static void main(String[] args) {
             
		This1 obj1 =new This1();
		obj1.test5();
		System.out.println("----------------");
		obj1.test1();
		

	}

}
