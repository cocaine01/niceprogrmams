package This;

public class ThisUses {

	int a;
	int b;
	
	void test1(int a,int b){
		a=a; //JVM is not understanding
		this.b=b;
	}
	void display(){
		System.out.println("Value of a and b  " +a+ "  , "+b);
	}
	
	void test2(int P, int Q){
		a=P;
		b=Q;
	}
	
	
	
	public static void main(String[] args) {
		ThisUses obj = new ThisUses();
//		obj.test1(4, 5);
//		obj.display();
		
		obj.test2(9, 9);
		obj.display();

	}

}
