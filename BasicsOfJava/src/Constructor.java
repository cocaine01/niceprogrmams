
public class Constructor {
	
	//we always create object of the constructor of the class
	// Advantage of Parameterised Constructor is that
	// We pass values to the object and 
	//* we can initialize the instance variables this.x=x;
	 //relation mapping between global and local variable..local(inside the bracket)
	
	//no 2 default constructor as JVM gets confused which one to call
	//Parameterized can be done but local variable -passing arguments data type is different
	//When a class extends other class , then also Compiler will first call the default constructor of 1st class and then 
	//--the second child class implementing in the 'super' method even if the super is not written.
	
	//Also in Child class, you can write super(5--with parameters) to call the parameterized constructor over there
	
	/*FIRST LINE OF CONSTRUCTOR is either this() or super() calling statement*/
	
	//Instance Variable or Global variable
	int a;
	int b;
	
	int z;
	
	public Constructor(){
		System.out.println("I am default Constructor");
		
	}
	//Local Variable//(inside the bracket)
	public Constructor(int a,int b){
		System.out.println("I am Parameterised Constructor");
		System.out.println("Value iof z now is:-" +z);
	}	
	
	//Initialization of the Variables and passing values when creating the Object
	public Constructor(int z){
		this.z=z;              //relation mapping between global and local variable..local(inside the bracket)
		System.out.println("Benefits of Parameterised constructor");
		System.out.println(z);
	}
	public Constructor(double z){
		//this.z=z;              //relation mapping between global and local variable..local(inside the bracket)
		System.out.println("Benefits of Parameterised constructor");
		System.out.println(z);
	}
	public Constructor(int z,int j,int t){
		this.z=z;              //relation mapping between global and local variable..local(inside the bracket)
		System.out.println("Benefits of Parameterised constructor");
		System.out.println(z);
	}

	
	void display(){
		System.out.println("Value of z is " + z);
	}
	
	
	public static void main(String[] args) {

//            Constructor obj =new Constructor();
            
//            Constructor obj1 =new Constructor(4,5);
            
            Constructor obj2 =new Constructor(10);
            
            obj2.display();
            
            
	}

}
