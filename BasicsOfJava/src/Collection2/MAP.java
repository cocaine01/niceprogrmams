package Collection2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MAP {
	
	/*
	 * MAP is an Interface in Java, so it has unimplemented methods
	 * So, we must have class to implement that MAp Interface.So, HashMap is a class which implements MAP.
	 * Map extends Collection Interface but not the static members.
	 * Store value in a MAP in Key and Value PAIR.
	 * MAP allows to store only one null key ,since duplicate Keys are not allowed.
	 * Internally it uses Node Concept
	 * 
	 * No Insertion Order
	 * 
	 * 
	 */

	public static void main(String[] args) {
		
		
		Map<Integer, String> hashMap = new HashMap<Integer, String>();
		
		//hashMap.put(null, "sumeet");//Onluy one null key allowed
		hashMap.put(1, "test");
		hashMap.put(2, "test");
		hashMap.put(1, "xyz"); ///Key+ Overiding of Data
		hashMap.put(5, "test");
		hashMap.put(null, "sumeet");//Onluy one null key allowed
		System.out.println(hashMap);
		
		System.out.println("=============");
		
		System.out.println(hashMap.get(2));
		System.out.println("=============");
		
		
		//In Order to iterate 
		Set<Integer> keyset= hashMap.keySet();
		Iterator<Integer> itr = keyset.iterator();
		//System.out.println(keyset.iterator());
		
		//One Way
		while (itr.hasNext()){
			//System.out.println(itr.next());
			System.out.println(hashMap.get(itr.next()));
		}
		
		System.out.println("=====FOR Values LOOP========");
		Collection<String> ks = hashMap.values();
		Iterator<String> itr2 = ks.iterator();
		while (itr2.hasNext()){
			System.out.println(itr2.next());
		}
		
		System.out.println("=====FOR EACH LOOP========");
		//2nd way : For Each Loop
		
		for (Map.Entry<Integer, String> abc : hashMap.entrySet()){
			System.out.println(abc.getKey()+"----"+abc.getValue());
		}
				
		
	}

}
