package Interface;

public class Example2 implements Interface1 ,InterfaceExample1 {

	
	void fromClass(){
		 System.out.println("Firefox from current class");
	}
	
	@Override
	public void test1() {
	  System.out.println("from 1st Interface");
		
	}

	@Override
	public void test2() {
	
		
	}
	@Override
	public void  test2ndInterface(){
		System.out.println("test2ndInterface");
	}

	@Override
	public void test4() {
		
		
	}

	public static void main(String[] args) {
		
		Interface1 obj= new Example2(); ///Reference of the Interface
		obj.test1();
		//obj.fromClass();--Not able to call from the current class.
		Example2 obj2=new Example2();
		obj2.test1();
		obj2.fromClass();

		obj2.test2ndInterface(); ///able to access all the methods of both the Interfaces		
	}
	

}
