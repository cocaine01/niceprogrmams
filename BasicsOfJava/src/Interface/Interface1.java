package Interface;



/* 
 * Interface is a mechanism where we can achieve 100% abstraction.
 * Interface has STATIC constants and abstract methods only.
 * Interface members/variables are by default public ,static and final.
 * Interface methods are 100% abstract means NON- IMPLEMENTED.so, no point of creating the object of 
   the Interface as it contains no methods.
 * Interface supports Multiple Inheritance----HOW ????
 * Agreement between client and company
 * 
 * We cannot create Object of Interface but can create the reference of Interface( WebDriver driver = new FirefoxDriver();
 * WHY WE CANNOT CREATE oBJECT BECAUSE THE METHODS AND VARIABLES ARE STATIC AND TO CALL THEM DIRECTLY CLASS NAME IS REQUIRED
 * 
 * Inter,ABs and Final----Only the Unimplemented methods comes at last class forcefully which extends the Abstract class
 *
 *Object creation and then overiding the method possible by anonymous inner class
 *same applies for abstraction
 */




public interface Interface1 {
	
	int i=10;      ///Need to initialize as because they are final and so it requires.	
	public static final int j=20; //Actually it is like this but public static and final are not written normally
   
	void test1();  //Unimplemented bodies with no content
	
	void test2();  //Method without body
	
	void test4();
	
}
