package InnerClass;

public class AnonymousInnerClass {
	
	/*
	 * Class with no Name and is used to overide the method or Interface
	 * Can be created in 2 ways
	 * inside the object only implementation is being done
	 * 
	 * 
	 */

	 class AC{
		
	}
	public static void main(String[] args) {
		
		Interface obj =new Interface(){
			
			@Override
			public void run()
			{
				System.out.println("I am overriding the method run present in the interface");
			}

			public void play() {
				System.out.println("Aise hi");
				
			}
		};
		
		
		obj.run();
		obj.play();
		
		System.out.println("----------Abstract Anonymous class-------");
		
		
		Abstract obj2=new Abstract(){
			
		@Override
		  public void absrun(){
			 System.out.println("Overiding the Abstract class");
		  }
		};
		
		obj2.absrun();
		
        
        
	}

}
