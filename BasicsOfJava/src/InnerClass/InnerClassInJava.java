package InnerClass;

class InnerClassInJava {	
	/*
	 * Inner Classes :- class within the class 
	 * Advantage:- Code is more readable
	 * any class having private member so cannot be accessed
	 * but Inner class can access the private members of outer class
	 * More Security and Code maintenance is easy
	 * through reference of Outer class, we cannot access the members/methods of inner class
	 */	
	
	class inner1{
		
		void test1(){
			System.out.println("I am from the method inside the inner class but learn how to access me");
			System.out.println("---------------Inner class accessing the member of outer class-------");
			OuterMethod();	//Method outside the inner class//try to access it
		}
	}
	
	//Method outside the inner class//try to access it
	public void OuterMethod(){
		System.out.println("I am the method from Outside inner class and inner method will try to access me");
	}
	
	//Local Inner Class.....method ke andar class and then uske andar method
			void LocalObj(){
			 class LocalInnerClass{
				 void message(){
					 System.out.println("Local Inner method");
				 }
			 }
			 LocalInnerClass l=new LocalInnerClass();
			 l.message();
		}
		
	//Static inner class
		static class StaticClass{
			 void StaticMethod(){
				System.out.println("From Static class");
			}
		}
			

	

	public static void main(String[] args) {
		
		InnerClassInJava obj=new InnerClassInJava();
		
		InnerClassInJava.inner1 innerobj= obj.new inner1();//Very Important//reference of inner class
		
		innerobj.test1();
		
		System.out.println("-----------------Local Inner class, method, class, then method, and object creation---------");
		
		obj.LocalObj();
		
		System.out.println("---------static calling----");		
		InnerClassInJava.StaticClass sc=new InnerClassInJava.StaticClass();
        sc.StaticMethod();
		
		
		
		
		
		
		
	}

}
