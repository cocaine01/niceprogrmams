package TEstNG;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNGNaveenBasics3 {
	@BeforeSuite 
	public void setUp(){
		System.out.println("@BeforeSuite Chrome Browser initialized3");
	}
	
	       @BeforeTest
	       public void login(){
		   System.out.println("@BeforeTest Launch Browser3");
	       }
	                @BeforeClass
	                 public void LaunchBrowser() {
		             System.out.println("@BeforeClass Enter the URL3");
	                 }
	                    @BeforeMethod
	                     public void enterURL(){
		                 System.out.println("@BeforeMethod Enter the credentials3");
	                     }	  
	                             @Test
	                              public void gogleTitleTest(){
		                          System.out.println("@Test Assertion of title3");
	                              }	 
	                             @Test
	                              public void SearchTest(){
		                          System.out.println("@Test SearchTest3");
	                              }	
	
	                     @AfterMethod
	                     public void logOut(){
		                 System.out.println("@AfterMethod logOut from App3");
	                     }
	                 @AfterClass
	                 public void deleteCookies(){
		             System.out.println("@AfterClass deleteCookies3");
	                 }
	         @AfterTest
	         public void closeBrowser(){
		     System.out.println("@AfterTest closeBrowser3");
	         }
	  @AfterSuite
	  public void GenerateReport(){
		  System.out.println("@AfterSuite GenerateReport3");
	  }
}
