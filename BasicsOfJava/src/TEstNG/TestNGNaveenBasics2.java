package TEstNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNGNaveenBasics2 {
	
	/*
	 * Types of Annotations:- Pre-requisite ,@Test and post requirements
	 * Method and @test come in pair
	 * STCM@T
	 */
	
	@BeforeSuite 
	public void setUp(){
		System.out.println("@BeforeSuite Chrome Browser initialized 2");
	}
	
	       @BeforeTest
	       public void login(){
		   System.out.println("@BeforeTest Launch Browser 2");
	       }
	                @BeforeClass
	                 public void LaunchBrowser() {
		             System.out.println("@BeforeClass Enter the URL 2");
	                 }
	                    @BeforeMethod
	                     public void enterURL(){
		                 System.out.println("@BeforeMethod Enter the credentials 2");
	                     }	  
	                             @Test
	                              public void gogleTitleTest(){
		                          System.out.println("@Test Assertion of title 2");
	                              }	 
	                             @Test
	                              public void SearchTest(){
		                          System.out.println("@Test SearchTest 2");
	                              }	
	
	                     @AfterMethod
	                     public void logOut(){
		                 System.out.println("@AfterMethod logOut from App 2");
	                     }
	                 @AfterClass
	                 public void deleteCookies(){
		             System.out.println("@AfterClass deleteCookies 2");
	                 }
	         @AfterTest
	         public void closeBrowser(){
		     System.out.println("@AfterTest closeBrowser 2");
	         }
	  @AfterSuite
	  public void GenerateReport(){
		  System.out.println("@AfterSuite GenerateReport 2");
	  }
	  
	  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	
//  @Test(dataProvider = "dp")
//  public void f(Integer n, String s) {
//  }
//  @BeforeMethod
//  public void beforeMethod() {
//  }
//
//  @AfterMethod
//  public void afterMethod() {
//  }
//
//
//  @DataProvider
//  public Object[][] dp() {
//    return new Object[][] {
//      new Object[] { 1, "a" },
//      new Object[] { 2, "b" },
//    };
//  }

//
//  @AfterClass
//  public void afterClass() {
//  }
//
//  @BeforeTest
//  public void beforeTest() {
//  }
//
//  @AfterTest
//  public void afterTest() {
//  }
//
//  @BeforeSuite
//  public void beforeSuite() {
//  }
//
//  @AfterSuite
//  public void afterSuite() {
//  }

}

