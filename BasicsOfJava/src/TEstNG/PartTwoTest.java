package TEstNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PartTwoTest {
	

	WebDriver driver = null;
	
	@BeforeMethod
	public void setUp(){
		driver = new FirefoxDriver();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	  @Test(priority=1)
	  public void googleTitleTest() {
		driver.get("https://www.google.co.in/");
		String title =driver.getTitle();
		System.out.println("Title is   :- "+title);
		
	  }
	  @Test(priority=5)
	  public void googleLogo() {
		driver.get("https://www.google.co.in/");
		boolean logo =driver.findElement(By.xpath("//img[@id='hplogo']")).isDisplayed();
	  }
	  @Test(priority=4, groups = {"COP"})
	  public void test1(){
		  System.out.println("test1 priority=4");
	  }
	  @Test(priority=3, groups = {"Cease"}, dependsOnGroups = {"COP"} )
	  public void test2(){
		  System.out.println("test2 priority=3");
	  }
	  @Test(priority=2 , dependsOnMethods="test1")
	  public void test3(){
		  System.out.println("test3 priority=2");
	  }
	  
	
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}

}
