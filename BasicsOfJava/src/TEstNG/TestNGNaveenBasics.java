package TEstNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNGNaveenBasics {
	
	/*
	 * Types of Annotations:- Pre-requisite ,@Test and post requirements
	 * Method and @test come in pair
	 * STCM@T
	 * 
	 * Sequencing set by Priority list(priority=1)
	 * These are called as Attributes of annotations
	 * 
	 * dependsOnGroups and Methods only available
	 * invocationTimeOut=30000
	 * expectedExceptions=NumberFormatException.class
	 * 
	 */
	
	@BeforeSuite 
	public void setUp(){
		System.out.println("@BeforeSuite Chrome Browser initialized");
	}
	
	       @BeforeTest
	       public void login(){
		   System.out.println("@BeforeTest Launch Browser");
	       }
	                @BeforeClass
	                 public void LaunchBrowser() {
		             System.out.println("@BeforeClass Enter the URL");
	                 }
	                    @BeforeMethod
	                     public void enterURL(){
		                 System.out.println("@BeforeMethod Enter the credentials");
	                     }	  
	                             @Test
	                              public void gogleTitleTest(){
		                          System.out.println("@Test Assertion of title");
	                              }	 
	                             @Test
	                              public void SearchTest(){
		                          System.out.println("@Test SearchTest");
	                              }	
	
	                     @AfterMethod
	                     public void logOut(){
		                 System.out.println("@AfterMethod logOut from App");
	                     }
	                 @AfterClass
	                 public void deleteCookies(){
		             System.out.println("@AfterClass deleteCookies");
	                 }
	         @AfterTest
	         public void closeBrowser(){
		     System.out.println("@AfterTest closeBrowser");
	         }
	  @AfterSuite
	  public void GenerateReport(){
		  System.out.println("@AfterSuite GenerateReport");
	  }
	  
	  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	
//  @Test(dataProvider = "dp")
//  public void f(Integer n, String s) {
//  }
//  @BeforeMethod
//  public void beforeMethod() {
//  }
//
//  @AfterMethod
//  public void afterMethod() {
//  }
//
//
//  @DataProvider
//  public Object[][] dp() {
//    return new Object[][] {
//      new Object[] { 1, "a" },
//      new Object[] { 2, "b" },
//    };
//  }

//
//  @AfterClass
//  public void afterClass() {
//  }
//
//  @BeforeTest
//  public void beforeTest() {
//  }
//
//  @AfterTest
//  public void afterTest() {
//  }
//
//  @BeforeSuite
//  public void beforeSuite() {
//  }
//
//  @AfterSuite
//  public void afterSuite() {
//  }

}
