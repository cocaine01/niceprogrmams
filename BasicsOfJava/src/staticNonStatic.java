
public class staticNonStatic {
	
	
	int j;
	int k;
	
	static int x;
	static int y;
	
	public void test1(){
		System.out.println("test  is non-static");
	}	
	public static void test2(){
		System.out.println("test  is static");
	}	
	public void test3(){
		System.out.println("test  is non-static");
	}	
	public static void test4(){
		System.out.println("test  is static");
	}
	
	public void understand1(){
		//can call both static and non-static variables in the method
		System.out.println(j);
		System.out.println(x);
	}
	public static void understand2(){
		//can call only static and NO  non-static variables in the method
		//Compile time error System.out.println(j);
		System.out.println(x);
	}
	

	public static void main(String[] args) {
		
		
		//Accessing only by object member
		staticNonStatic obj1=new staticNonStatic();
		obj1.test1();
		obj1.test3();
		System.out.println(obj1.k);
		System.out.println(obj1.j);
		
		//Accessd by both object member and directly class--STATIC
		staticNonStatic.test2();
		staticNonStatic.test4();		
		System.out.println(staticNonStatic.x);
		System.out.println(staticNonStatic.y);
		staticNonStatic.understand2();
		
		
		}

}
