package parametersTestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterTest {
	
	WebDriver driver = null;
	
  @Test
  @Parameters({"url","username"})
  public void YahooTest(String url,String username) {
	  System.setProperty("webdriver.chrome.driver" , System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.get(url);
	  driver.findElement(By.xpath("//input[@name='username']")).sendKeys(username);	  
  }
}
