
public class classAndObject {
	
	//Properties
	int height =15;
	int weight =10;
	
	//Method
	public void walk(){
		System.out.println("walk()");
	}
	
	public void eat(){
		System.out.println("eat()");
	}
	
	
	
	

	public static void main(String[] args) {
        
		//Creating Object
		classAndObject obj=new classAndObject();
		
		//Method calling
		obj.walk();
		obj.eat();
		
		//getting the Properties
		System.out.println(obj.height);
		

	}

}
