package Sigleton;

public class Satellite {
	
	static int counter = 0;
	
	private Satellite() {
		System.out.println("Constructor invoked");
		counter++;
		System.out.println("Value of counter "+counter);
	}
	
	private static Satellite instance;
	
	public static Satellite getSatellite() {
		if(instance==null)
		//if(counter < 3 ) 
		{
			instance = new Satellite();
		}
		
		return instance;
	}
	
	
	

	public static void main(String[] args) {
		//Satellite s = new Satellite();
		for(int i=0; i<= 5;i++) {
			System.out.println(Satellite.getSatellite().hashCode());
		}

	}

}
