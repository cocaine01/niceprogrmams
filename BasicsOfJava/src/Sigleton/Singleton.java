package Sigleton;

public class Singleton {
	
	private static Singleton instance;
	
	static int counter =0;
	
	
	private Singleton() {
		System.out.println("Constructor invoked and counter value "+counter);
		counter++;		
	}
		
	private static Singleton getInstance() {		
		if(counter < 4)
		//if(instance ==null) 
		{
			instance = new Singleton();
		}
		return instance;
	}
	public static void main(String[] args) {
		
		
		for (int i=0;i<=6;i++) {
			System.out.println(Singleton.getInstance().hashCode());
		}

	}

}
