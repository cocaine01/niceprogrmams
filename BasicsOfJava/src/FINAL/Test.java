package FINAL;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Test extends Final{
	
	
	public void override(){
		System.out.println("From Child class");
	}

	public static void main(String[] args) {
		
		Test ob= new Test();
		ob.override();
		System.out.println("**********************");
		ob.test();
		
		System.out.println("**********************");
		
		
		
		final List<String> loans= new ArrayList<String>();
		
		loans.add("home loan");
		System.out.println(loans);
		
		///loans = new Vector();////so it cannot be done as the Final 
		
		
		
		
		
		
	    
		

	}

}
