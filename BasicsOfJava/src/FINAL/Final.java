package FINAL;

public class Final {
	
	/* A FINAL class cannot be extended or Inherited by another class.( Matlab ki Extends wala nahi chalta for Final CLASS)
	 * Benefit:- We cannot the modify the members of Final from external class.
	 * String class is a final class
	 * Wrapper class is also are final class like Integer, Boolean, Float
	 * Making class Final improves performance of the JVM.
	 * 
	 * All members of Interface are Final only.
	 * Final Methods cannot be Over-ridden but Inherited.( Extends wala)
	 * Variable if Final then cannot be re-assigned
	 * 
	 * 
	 * Class:_ A FINAL class cannot be extended or Inherited by another class.( Matlab ki Extends wala nahi chalta for Final CLASS)
	 * 
	 * Me5thod:- Final Methods cannot be Over-ridden but Inherited.( Extends wala)
	 * 
	 * Variable:-Variable if Final then cannot be re-assigned.
	 * If variable not initialialized then can be done by Constructor
	*/
	
	
	int i=10;
	final int j=20;
	
	final int x;	
	Final(){
		x=90;
	}
	

	
	final void test(){
		System.out.println("I am a final Class method so I can be Inherited but not Overidden");
	}
	
	public void override(){
		System.out.println("From Parent class");
		i=50;
		/////j=50; not possible
		
	}
	
	
	
	

	public static void main(String[] args) {
		

	}

}
