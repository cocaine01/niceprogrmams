package OverloadingOveriding;

public class OverridingChild extends  OverRidingParent{
	
	public void test1(){
		System.out.println("Child Test 1");
	}
	
//	Not possible to override the final method while writing in the child class
//	final void test3(){
//		System.out.println("Final Method");
//	}
	
	//Compile time error when final method with same signature is invoked in the Child Class as well 
	//although you can call the final method in child class
	/*final void test3(){
		System.out.println("Final Method --you cannot write the same method in the child class");
	}*/
	private void test4(){
		System.out.println("Private cHILD");
	}
	
	public static void test5(){
		System.out.println("Static Child");
	}	

	public static void main(String[] args) {
 
		OverridingChild child =new OverridingChild();
		
		child.test1(); 
		///child class given more preference.although test1() is present in both child and Parent
		//test 1() implementations have been changed.
		child.test2(); //since method not present in Child so went to Parent
		
		child.test3();
		//ch.test4(); //since it is private so it cannot be override--Duplicate in Child Class
		
		OverRidingParent.test5();
	/*Notes
	 * Inheritance is a must for Method Overriding so extends is mandate.
	 * child class given more preference.although test1() is present in both child and Parent
		//test 1() implementations have been changed.
	 * You cannot call the private methods in the child class.
	 * 	Compile time error when final method with same signature is invoked in the Child Class as well 
	//although you can call the final method in child class
	 * Static method needs to be called only by the class names.(class name.methodName)
	 * //being called from Parent class since Static members are always called  from Parent class  and they dont participate 
		//in the Overriding Method************IMP****
	 */
		
		OverRidingParent parent = new OverridingChild();
		parent.test5();//being called from Parent class since Static members are always called  from Parent class  and they dont participate 
		//in the Overriding Method
	
		
		
	}

}
