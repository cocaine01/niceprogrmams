package OverloadingOveriding;

public class Overloading {

	// Method Overloading:-More than one Method with the same name by changing
	// the argument to the method
	// One start system for all vehicles but way of starting is different for every vehicle
	// Changing the type of argument also helps
	/// 2 ways of Method Overloading---changing the number or type of the argument
	// * CHNAGING the return type of the method does not help in Overloading
	// //overloading the final method is possible.
	 //overloading the static method is also possible	
	
	

	public void test1() {
		System.out.println("test1()");
	}

	public void test1(int i) {
		System.out.println("test1(int i)");
	}
	
	//Changing the type of argument
	public void test1(double i) {
		System.out.println("test1(same name but type of argument different)(double i)");
	}
	
	public void test1(int i, int j) {
		System.out.println("test1(int i, int j)");
	}

	public void test1(int i, int j, int k) {
		System.out.println("test1(int i, int j,int k)");
	}
	
	
	public final void testFinal(){	}
    public final void testFinal(int i){		}
    
  //overloading the static method is also possible	
  	public static void testStatic(){	}
      public static void testStatic(int i){		}
      
      
	

	public static void main(String[] args) {
		Overloading obj = new Overloading();
		obj.test1();
		obj.test1(2);
		obj.test1(1, 1);
		obj.test1(3, 3, 3);
		obj.test1(9.8);
	}

}
