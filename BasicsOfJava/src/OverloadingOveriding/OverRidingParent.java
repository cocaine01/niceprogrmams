package OverloadingOveriding;

public class OverRidingParent {

	
	//Can be achieved only by Inheritance
	
	public void test1(){
		System.out.println("Parent test 1");
	}
	public void test2(){
		System.out.println("Parent test 2");
	}
	
	
	final void test3(){
		System.out.println("Final Method --you cannot write the same method in the child class");
	}
	private void test4(){
		System.out.println("Private");
	}
	
	public static void test5(){
		System.out.println("Static Parent");
	}	
	


}
