package String;

public class Tirath {

	public static void main(String[] args) {
		
		
		String s1 =  "T";
		String s2 =  "T";
		
		String s3 = s1;
		
		System.out.println(s1==s2);
		System.out.println(s2==s3);
		System.out.println(s3==s1);
		System.out.println(s1.equals(s2));
		System.out.println(s2.equals(s3));
		System.out.println(s3.equals(s1));
		System.out.println(s1.equals(new String("T")));
		System.out.println(s1== new String("T"));
	}

}
