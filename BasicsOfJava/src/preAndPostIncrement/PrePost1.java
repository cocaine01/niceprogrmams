package preAndPostIncrement;

public class PrePost1 {
	
	//Pre :- Increment the value of variable and then use
	//Post:- use the value and then increment
	
	public static void main(String[] args) {
		int i =0;
		int j=0;
		
		int m=0;
		int n=0;
		//Simple//
		System.out.println("Pre Increment of i is : " + ++i);
		System.out.println("Pre Increment of j is : " + ++j);
		
		System.out.println("Post Increment of m is : " + m++);
		System.out.println("Post Increment of n is : " + n++);
		
		System.out.println("Value of m and n is now : " +m+ ","  +n);
		
		///Special//
		
		int x=0;
		int y=0;
		int z=2;
		
		int k= x++;
		
		int l =++y;
		
		int p=z--;
		int q=--z;
		
		
		System.out.println(k);
		System.out.println(l);
		System.out.println(k);
		
		System.out.println(p);
		System.out.println(q);
	}

}
