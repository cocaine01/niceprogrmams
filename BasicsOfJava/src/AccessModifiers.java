public class AccessModifiers {

	public int i;
	private int j; //Private cannot be accessed in other class
	protected int p; ///can be accessed in another Package by Inheritance--Extends
	int d; // /default//within the package it can be accessed

	public void test1() {
      System.out.println("public");
	}
    //Private cannot be accessed in other class
	private void test2() {
		 System.out.println("private");
	}

	protected void test3() {
		 System.out.println("protected");
	}

	 void test4() {
		 System.out.println("Default");
	}

	 public static void main(String[] args) {
		 AccessModifiers obj=new AccessModifiers();
		 obj.test1();
		 obj.test2();
		 obj.test3();
		 obj.test4();
		 
		 System.out.println(obj.i);
		 
		 
	}
}
