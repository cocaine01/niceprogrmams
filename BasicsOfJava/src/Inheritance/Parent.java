package Inheritance;

public class Parent {
	
	
//	INHERITANCE:- (Child extends Parent)-can only call public and PROTECTED methods.
//    All kinds of variables can be called in the child class except private.
//
///Inheritance of static members is not possible
//cannot inherit the private members or variables of the Parent class
//can call public and PROTECTED members
	 public int i;
	 private int x;
	 protected int z;
	 static int a =10;
	 
	
	
	
	public void test1(){
		System.out.println("Parent class 1");
	}

	protected void test3(){
		System.out.println("Parent Protected class 3");
	}
	
	public static void testStatic(){
		System.out.println("I am static");
	}
	
	
	public static void main(String[] args) {
		
		

	}

}
