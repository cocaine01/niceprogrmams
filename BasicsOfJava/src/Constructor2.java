
public class Constructor2 extends Constructor {
	
	//Instance Variable or Global variable
	int a;
	int b;
	int c;
	
	public Constructor2(){
		super();
		System.out.println("Chile Default constructor");
	}
	
		//Local Variable//(inside the bracket)
	public Constructor2(int a, int b, int c){
		this();///calling from itself class
		System.out.println("Chile Parameterised Constructor");
		this.a=a;
		this.b=b;
		this.c=c;
		
	}

	public void display(){
		System.out.println( "The value of a,b,c is :- " +a+ ","+b+","+c);
	}
	
	public static void main(String[] args) {
		Constructor2 obj=new Constructor2(1,2,3);
		obj.display();

	}

}
