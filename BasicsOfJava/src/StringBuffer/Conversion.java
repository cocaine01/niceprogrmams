package StringBuffer;

public class Conversion {

	public static void main(String[] args) {
		String str ="Sumeet";
		
		StringBuffer sb = new StringBuffer(str);
		System.out.println(sb);
		
		StringBuilder sbui = new StringBuilder(str);
		System.out.println(sbui);
		System.out.println("--------------------");
		
		StringBuffer sStringBuffer = new StringBuffer("StringBuffer");
		System.out.println(sStringBuffer);
		
		StringBuilder sbui1 = new StringBuilder(sStringBuffer);
		System.out.println(sbui1);
		
		
		System.out.println("Conversion to String "+sStringBuffer.toString());
		
		

	}

}
