package AbstractClass;

public abstract class AbstractClass1 {
	
	/*
	 * Keyword Abstract --class can have both abstract and non-abstract methods.Method Implementation
	 * body for abstract method is not allowed
	 * cANNOT CREATE THE oBJECT OR instatANIATE THE aBSTRAct CLASS.
	 * Moment we create an object , Object will get copy of all non-static members
	 * Any one member described as abstract then automatically it becomes a Abstract Class
	 * Variable cannot be abstract.
	 * EXTEND keyword--when extended only the unimplemented methgods will be called in the Child class.Also with child class obj, we can 
	 * call the non-abstract method
	 */
	void test1() {
		System.out.println("nON-aBSTRATC CONCRETE MEthod---means Implemented");
	}
	
	abstract void test2(); //Abstract with no body and no implementation
	
	
	
	

	public static void main(String[] args) {
	//	AbstractClass1 d= new AbstractClass1();
		
 
	}

}
