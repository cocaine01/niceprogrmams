public class returnTypeINJava {

	public void test1() {
		System.out.println("Not return anything");
	}

	public int test2() {
		System.out.println("Return Integer");
		return 5;
	}

	public double test3() {
		return 9.2;
	}

	public boolean test4() {
		return false;
	}

	public static void main(String[] args) {
       int[] arr     =new int[3];
       int[][] D= new int[3][4];
       
	}

}
